## Trabajo Práctico de Socket - Sistemas Distribuidos

### Integrantes
- Rafael Ricardo - 4.357.454
- Rodrigo Amarilla - 3.460.079

### Tema
Tipo de Socket: Ciertas operaciones con TCP y otras con UDP

### Link de videos explicativo del trabajo
### Video Rafael Ricardo
https://drive.google.com/file/d/1WQv92zFGCwvarBayLAxBo6gVBeAemar0/view?usp=sharing

### Video Rodrigo Amarilla
https://drive.google.com/file/d/1ZGnii2gAERALwrmzUOU5N5a_VqTjaMIc/view?usp=sharing

### Elementos a utilizar
- openjdk version "11.0.16"
- IDE Eclipse
- Instalar el motor de base de datos Postgresql

### Descargar el proyecto
###### Via SSH:Via SSH:

	git@gitlab.com:Rafael90/proyecto_socket.git

###### Via HTTPS:

	https://gitlab.com/Rafael90/proyecto_socket.git

### Importar proyecto
- Abrir el IDE Eclipse
- Menú File -> Import -> Existing Maven Project
- Dentro de los archivos descargados, especificar la carpeta "proyecto_socket"
- Seleccionar la carpeta server_nis
- Finalizar
- El IDE comenzará a importar las librerías de Maven
- De la misma manera hacemos para abrir el proyecto maven Cliente seleccionando la carpeta Cliente

### Base de datos
Crear una base de datos **server_nis**
Configurar los datos de configuración y acceso en la clase "Bd.java".

```sql
CREATE SEQUENCE public.server_nis;
```

```sql
CREATE TABLE public.nis
(
	nis_cliente integer NOT NULL,
	consumo integer NOT NULL,
	nombre character varying(20),
	apellido character varying(20),
	correo character varying(20) NOT NULL,
	CONSTRAINT pk_nis_cliente PRIMARY KEY (nis_cliente)
);

```
```sql

```
### Ejecupar Servidor server_nis y cliente
```
/server_nis/src/main/java/py/una/pol/server/TCPMultiServer.java
```
```
/server_nis/src/main/java/py/una/pol/server/UDPServer.java
```
```
/Cliente_nis/src/main/java/py/una/pol/server/Cliente.java
```

### Servicios del Servidor
- ###### Registro de consumo:
```json
{
	"nis_cliente" : {
		"consumo" : "2000",
		"apellido" : "Perez",
		"correo" : "jp@mail.com",
		"nombre" : "Juan",
		"nis_cliente" : 3423
	},
	"metodo" : "1"
}
```
Respuesta:
```json
{
	"estado" : "0",
	"mensaje" : "Se registró con éxito"
}
```
- ###### Conexión:
```json
{
	"session" : {
		"correo" : "jp@mail.com",
	},
	"metodo" : "2"
}
```
Respuesta:
```json
{
	"estado" : "0",
	"nis" : {
		"consumo" : 2000,
		"nis_cliente" : "3423",
		"apellido" : "Perez",
		"correo" : "jp@gmail.com",
		"nombre" : "Juan"
	},
	"mensaje" : "Conexión exitosa."
}
```
###### Lista de conectados

```json
{
	
	"metodo":"3"
}

```

	Respuesta:
```json

	Servidor: 
	{
		"estado":"0",
		"Nis":{
			"consumo":2000,
			"apellido":"Perez",
			"correo":"jp@gmail.com",
			"nombre":"Juan",
			"nis_cliente":3423
		},
		"mensaje":"Conectados"
	}
```
###### Desconexión

```json
{
	"correo":"jp@gmail.com",
	"metodo":"4"
}
```

	Respuesta:
```json

Servidor: {
			"estado":"0",
			"mensaje":"Desconexión exitosa"
		  }
```