package py.una.pol.template;

import java.util.List;

//import py.una.pol.model.Mensaje;
import py.una.pol.model.Nis;

public class Ventana {
	
	
	public static void ventanaPrincipal() {
		System.out.println("╔══════════════════════════════════════╗");
    	System.out.println("║ Bienvenidos                          ║");
    	System.out.println("║ 1. Registrar consumo                 ║");
    	System.out.println("║ 2. Conectar                          ║");
    	System.out.println("║ 3. Listar Conectados                 ║");
    	System.out.println("║ 4. Desconectar                       ║");
    	System.out.println("║  Muchas gracias                      ║");
    	System.out.println("╚══════════════════════════════════════╝");
    	System.out.print("> ");
	}
	
	public static void ventanaSesion() {
		System.out.println("╔══════════════════════════════════════╗");
    	System.out.println("║           Iniciar conexión           ║");
    	System.out.println("╚══════════════════════════════════════╝");
	}

	public static void ventanaMensajeRespuesta(String mensaje) {
		System.out.print("╔");
		for (int i = 0; i < mensaje.length()+10; i++) {
			System.out.print("═");	
		}
		System.out.println("╗");
    	System.out.println("║     " + mensaje + "     ║");
    	System.out.print("╚");
    	for (int i = 0; i < mensaje.length()+10; i++) {
			System.out.print("═");	
		}
    	System.out.println("╝\n");
	}
	
	public static void ventanaListaConectado(List<Nis> list, String correo) {
		System.out.println("╔══════════════════════════════════════╗");
    	System.out.println("║             Conectados               ║");
    	System.out.println("╚══════════════════════════════════════╝");
    	
    	int i = 1;
    	for (Nis nis : list) {
    		if (nis.getCorreo().compareTo(correo) != 0) {
    			System.out.println(i + ". " + nis.getNombre() + " " + nis.getApellido());
    			i++;
    		}
		}
	}
	
}
