package py.una.pol.service;

import java.sql.SQLException;
import java.util.List;

import py.una.pol.dao.NisDAO;
import py.una.pol.model.ConvertirJSON;
import py.una.pol.model.Nis;

public class NisService {
	
	private static NisDAO pdao = new NisDAO();
		
	public static String registrarConsumo(Nis nis) throws SQLException {	
		String estado = "0";
		String mensaje = "Se registró con éxito";
		
		//if (verificarNis_cliente(nis.getNis_cliente()) == 0 /*&& verificarCorreo(nis.getCorreo()) == 0*/) {
			long x = pdao.insertar(nis);
			System.out.println("key: " + x);
			
			if (x == -1) {
				mensaje = "Error: Indeterminado.";
				estado = "-1";
			}
		
		//}

		return ConvertirJSON.mensajeJSON(estado, mensaje);
	}
	
		
	public static String sesionNis(String correo) {
		String estado = "-1";
		String mensaje = "Error: Indeterminado.";
		
		if (verificarCorreo(correo) == 0) {
			estado = "1";
			mensaje = "No hay tal correo electrónico, no se pudo conectar.";
		} else {
			Nis nis = obtenerNisCorreo(correo);
			if (correo.compareTo(nis.getCorreo()) == 0) {
				estado = "0";
				mensaje = "Conexión exitosa.";
				return ConvertirJSON.mensajeDatoJSON(estado, mensaje, nis);
			} 
		}

		return ConvertirJSON.mensajeJSON(estado, mensaje);		
	}

	
	private static int verificarNis_cliente(Long nis_cliente) {
		List<Nis> lista = pdao.seleccionarPorNis_cliente(nis_cliente);
		
		return lista.size();
	}
	
	public static int verificarCorreo(String correo) {
		List<Nis> lista = pdao.seleccionarPorCorreo(correo);
		
		return lista.size();
	}
	
	public static Nis obtenerNisCorreo(String correo) {
		List<Nis> lista = pdao.seleccionarPorCorreo(correo);
		
		return lista.get(0);
	}
	
}
