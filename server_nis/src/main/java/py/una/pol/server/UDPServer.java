package py.una.pol.server;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import py.una.pol.model.ConvertirJSON;
import py.una.pol.model.ConvertirObject;
import py.una.pol.model.Nis;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class UDPServer {
	private static Logger logger = LogManager.getLogger(UDPServer.class);
	private static String mensajeImprimir;
	
	
	public static void main(String[] a){
        int puertoServidor = 9876;
        List<Nis> nisLista = new ArrayList<Nis>();
        String str = new String("Desconexión exitosa");
        String estadocerrar = new String("0");
        
        
        try {
        	Integer metodo;

            @SuppressWarnings("resource")
			DatagramSocket serverSocket = new DatagramSocket(puertoServidor);
			System.out.println("Servidor - UDP ");
			
            byte[] receiveData = new byte[1024];
            byte[] sendData = new byte[1024];
            
            String datoRecibido = "";
            String datoEnviado = "";
            
            
            while (true) {
                receiveData = new byte[1024];
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                System.out.println("______________________________________________");
                System.out.println("SERVIDOR UDP ACTIVO... ");
                serverSocket.receive(receivePacket);

                datoRecibido = new String(receivePacket.getData());
                datoRecibido = datoRecibido.trim();
                System.out.println("Cliente: " + datoRecibido);
                // Metodo 2: Registrar persona conectada
                if (ConvertirObject.obtenerMetodo(datoRecibido) == 2) {
					nisLista.add(ConvertirObject.obtenerNis(datoRecibido));
					
					
				// Metodo 3: Listar las personas conectadas
                } else if ((metodo= ConvertirObject.obtenerMetodo(datoRecibido) )== 3) {
                	
                	//metodo= ConvertirObject.obtenerMetodo(datoRecibido);
                	
                	mensajeImprimir = "la ip es: " +  receivePacket.getAddress() + " el metodo es : " + metodo + String.valueOf( new Date());
                	
                	logger.info(mensajeImprimir);
    				System.out.println(mensajeImprimir);
                	
                	
                	datoEnviado = ConvertirJSON.mensajeDatoJSONUDP("0", "Conectados", nisLista);
                	System.out.println("Servidor: " + datoEnviado);
                    InetAddress IPAddress = receivePacket.getAddress();
                    int port = receivePacket.getPort();                
                    sendData = datoEnviado.getBytes();
                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress,port);
                    serverSocket.send(sendPacket);
                // Metodo 4: Borrar un persona desconectada
				} 
                
                else if ((metodo=ConvertirObject.obtenerMetodo(datoRecibido)) == 4) {
                	
                	mensajeImprimir = "la ip es: " +  receivePacket.getAddress()  + " el metodo es : " + metodo + String.valueOf( new Date());
             
                	logger.info(mensajeImprimir);
    				System.out.println(mensajeImprimir);
    				
					int i=0;
					for (Nis nis : nisLista) {
						if (nis.getCorreo().compareTo((ConvertirObject.obtenerCorreo(datoRecibido))) == 0) {
							nisLista.remove(i);
							break;
						}
						i++;
					}
					
					datoEnviado = ConvertirJSON.mensajeJSON(estadocerrar, str);
                	System.out.println("Servidor: " + datoEnviado);
                    InetAddress IPAddress = receivePacket.getAddress();
                    int port = receivePacket.getPort();                
                    sendData = datoEnviado.getBytes();
                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress,port);
                    serverSocket.send(sendPacket);
					
                }
				
            }

        } catch (Exception ex) {
        	ex.printStackTrace();
            System.exit(1);
        }
    }
	
}  

