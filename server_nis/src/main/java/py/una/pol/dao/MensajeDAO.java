package py.una.pol.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.List;

import py.una.pol.model.Mensaje;

public class MensajeDAO {
	public long insertar(Mensaje m) throws SQLException {

		String consulta = "INSERT INTO mensaje(mensaje, estado) VALUES(?, ?)";

		long id = 0;
		Connection conn = null;

		try {
			conn = Bd.connect();
			PreparedStatement pstmt = conn.prepareStatement(consulta, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, m.getMensaje());
			pstmt.setString(2, m.getEstado());

			int affectedRows = pstmt.executeUpdate();
			if (affectedRows > 0) {
				try (ResultSet rs = pstmt.getGeneratedKeys()) {
					if (rs.next()) {
						id = rs.getLong(1);
					}
				} catch (SQLException ex) {
					System.out.println(ex.getMessage());
				}
			}
		} catch (SQLException ex) {
			System.out.println("Error en la insercion: " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (Exception ef) {
				System.out.println("No se pudo cerrar la conexion a BD: " + ef.getMessage());
			}
		}

		return id;
	}

}
